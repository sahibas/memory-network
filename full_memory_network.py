# -*- coding: utf-8 -*-

import numpy as np
import operator

# loads data
############################################
# Functions to read in the stories from file
############################################

def vectorize_string(string, word_to_index):
    """ Turns a string into a vector of word counts """
    num_words = len(word_to_index)
    vector = np.zeros([num_words, 1])
    for word in string.split(' '):
        vector[word_to_index[word.lower()]] += 1
    return vector

class Question (object):
    """ Represents a question (e.g. "Where is John?") 
    
    fields: self.linenum, self.question_text, self.answer_text, self.fact_linenum,
        self.question, self.answer, self.answer_index
    
    """

    def __init__(self, linenum, question_text, answer_text, fact_linenum):
        """ Create a question

        arguments:
            linenum (int) -- the line of the story, indexed from 1, where the question appeared
            question_text (string) -- the text of the question (e.g. "Where is John?")
            answer_text (string) -- the (one word) text of the answer (e.g. "hallway")
            fact_linenum (int) -- the line number where the supporting statement appeared (e.g. 4)

        """

        self.linenum = linenum
        self.question_text = question_text
        self.answer_text = answer_text
        self.fact_linenum = fact_linenum

    def vectorize(self, word_to_index):
        """ Represent the question text and answer text as bag of words vectors """
        self.question = vectorize_string(self.question_text[0:-1], word_to_index) # remove the question mark
        self.answer = vectorize_string(self.answer_text, word_to_index)
        self.answer_index = word_to_index[self.answer_text]

class Statement (object):
    """ Represents a statement (e.g. "John went to the hallway.") 

    Fields: self.linenum, self.statement_text, self.statement    
    
    """

    def __init__(self, linenum, statement_text):
        """ Create a statement

        arguments:
            linenum (int) -- the line of the story where the statement appeared (e.g. 2)
            statement_text (string) -- the text of the statement (e.g. "John went to the hallway")
        """

        self.linenum = linenum
        self.statement_text = statement_text

    def vectorize(self, word_to_index):
        """ Represent the statemement text as a count vector """
        self.statement = vectorize_string(self.statement_text[0:-1], word_to_index)  # remove the period


class Story (object):
    """ Represents a story

    A story consists of a bunch of questions and a bunch of statements.
    Question and statements are stored in dictionaries indexed by their line number.

    """

    def __init__(self):
        self.questions = {}
        self.statements = {}

    def vectorize(self, word_to_index):
        """Vectorizes all statements and questions in story"""
        for statement in self.statements.itervalues():
            statement.vectorize(word_to_index)
        for question in self.questions.itervalues():
            question.vectorize(word_to_index)

    def __str__(self):
        print "Story with %d questions and %s statements" % (len(self.questions), len(self.statements))

    def is_empty(self):
        return len(self.statements) == 0 and len(self.questions) == 0


def load_stories(filename):
    """ Loads and parses a stories file; returns a list of stories """ 

    file = open(filename)

    # a list of all stories
    stories = []

    # the story that we are currently reading in
    cur_story = Story()

    for line in file:
        # extract the line number
        split = line.split(' ')
        linenum = int(split[0]) #line in the story, from 1-15
        rest = ' '.join(split[1:])

        # if this is the beginning of a new story (i.e. the line number is 1),
        # wrap up the previous story and add it to the list
        if linenum == 1 and not cur_story.is_empty():
            stories.append(cur_story)
            cur_story = Story()

        # branch on whether this line is a question or a statement
        if '?' in rest: # it's a question
            (question_text, answer_text, fact_linenum) = [s.strip() for s in rest.split("\t")] # fields separated by tab
            cur_story.questions[linenum] = Question(linenum, question_text, answer_text, int(fact_linenum))
        else: # it's a statement
            statement_text = rest.strip()
            cur_story.statements[linenum] = Statement(linenum, statement_text)

    # add the last story to the list
    stories.append(cur_story)

    file.close();

    return stories

def make_dictionary(stories):
    """ Creates a "dictionary" from a list of stories. 

    A dictionary is a mapping between words and indices in bag of words form
    
    arguments:
        stories -- a list of stories (most likely combined from training + testing data sets)

    returns:
        word_to_index -- a dictionary that maps from words to indices (starting from 0)
        index_to_word -- a list of words; index_to_word[i] is the word represented by index i

    """ 
    word_set = set() # set of all words
    for story in stories:
        for statement in story.statements.itervalues():
            for word in statement.statement_text[0:-1].split(' '):
                word_set.add(word.lower())
        for question in story.questions.itervalues():
            for word in question.question_text[0:-1].split(' '):
                word_set.add(word.lower())
            word_set.add(question.answer_text.lower())

    index_to_word = list(word_set) # creates list of all words ordered by new index
    word_to_index = {}
    # creates dictionary (index to word mapping) from list
    for (index, word) in enumerate(index_to_word):
        word_to_index[word] = index

    return (word_to_index, index_to_word)

###################################
# Memory networks
###################################


def form_vector(num_words, question=None, fact=None, answer_index=None):
    """ Forms a 2|W| or 3|W| vector representation of a question/fact/answer by
    stacking the three bag of words vectors on top of each other.
    
    In the 2|W| vector for fact embedding, the first half of the vector is for 
    the question, and the second half for the fact.
    In the 3|W| vector for answer embedding, first third of the vector is for 
    the question, the second third for the fact, and the third third for the answer.
    
    arguments:
        num_words (int) -- size of vocabulary, |W|
        question -- optional |W| bag of words representation of question
        fact -- optional |W| bag of words representation of fact
        answer_index (int) -- optional index of answer (number from 0 to |W|-1)
    """
    if answer_index != None: # if there is an answer, then it is the only entry in 3|W| vector for answer embedding
        vector = np.zeros([num_words * 3, 1])
        vector[2*num_words + answer_index] = 1
    elif question == None: # if there is only a fact, then it is only entry in 2|W| vector for fact embeddding
        vector = np.zeros([num_words * 2, 1])
        vector[num_words:] = fact
    elif fact == None: # if there is only a question, then it is only entry in 2|W| vector for fact embedding
        vector = np.zeros([num_words * 2, 1])
        vector[0:num_words] = question
    else: # else there is a question and a fact in a 3|W| vector for answer embedding
        vector = np.zeros([num_words * 3, 1])
        vector[0:num_words] = question
        vector[num_words:2*num_words] = fact
    return vector

def score(x, y, embedding):
    """ Scores two question/fact/answer vectors given an embedding.

    The score between two q/f/a vectors is the dot product between
    their embeddings.

    arguments:
        x -- a 2|W| or 3|W| vector
        y -- a 2|W| or 3|W| vector
        embedding -- a D x 2|W| or D x 3|W| matrix, where D is the embedding dimension
     """

    return np.dot(np.dot(embedding, x).T, np.dot(embedding, y))

def score_deriv(x, y, embedding):
    """ Computes the derivative of the score between two vectors x and y
    with respect to the embedding.

    arguments:
        x -- a 2|W| or 3|W| vector
        y -- a 2|W| or 3|W| vector
        embedding -- a D x 2|W| or D x 3|W| matrix, where D is the embedding dimension

    returns: a D X 2|W| or 3|W| matrix
    """
    return np.dot(embedding, np.dot(y, x.T) + np.dot(x, y.T)) 

class Prediction (object):
    """ Represents a single prediction from our trained question/answer network """

    def __init__(self, fact_linenum, answer_index):
        """ Creates a prediction 

        arguments: 
            fact_linenum (int) -- the line number of the predicted fact
            answer_index (int) -- the dictionary index of the predicted one-word answer 

        """

        self.fact_linenum = fact_linenum
        self.answer_index = answer_index

def predict_answer(question, fact, num_words, embedding_answer):
    """ Predict the answer to a question, given the supporting fact

    arguments:
        question (length |W| vector) -- the question as a bag of words vector
        fact (length |W| vector) -- the supporting fact as a bag of words vector
        num_words (int) -- the number of words in the vocabulary
        embedding_answer (D X 3|W| matrix) -- the trained answer embedding matrix

    returns: the index of the predicted answer

    """

    # form the q/f/a vector for the question and fact
    question_vec = form_vector(num_words, question=question, fact=fact)

    # the score for each candidate answer
    answer_scores = {}
    for word_index in range(num_words):
        # form the q/f/a vector for this candidate answer
        answer_vec = form_vector(num_words, answer_index=word_index)

        # score this candidate answer
        answer_score = score(question_vec, answer_vec, embedding_answer)
        answer_scores[word_index] = answer_score
        
    # get the index of the candidate answer with the highest score, and return it 
    best_answer = max(answer_scores.iteritems(), key=operator.itemgetter(1))[0]
    return best_answer


def predict_fact(question, memory_bank, embedding_fact):
    """ Predict the supporting fact for a question 

    arguments:
        question (length |W| vector) -- the question as a bag of words vector
        memory_bank (dictionary) -- a dictionary containing all candidate supporting facts, indexed by their line number in the story
        embedding_fact (D X 2|W| matrix) -- the trained fact embedding matrix

    returns: the line number of the predicted supporting fact

    """
    
    # form the q/f/a vector for the question
    question_vec = form_vector(num_words, question=question)

    # a dictionary with the score for each candidate fact, indexed by line number
    fact_scores = {}
    for (fact_linenum, fact) in memory_bank.iteritems():
        # form the q/f/a vector for this candidate supporting fact
        fact_vec = form_vector(num_words, fact=fact)

        # score this candidate supporting fact
        fact_scores[fact_linenum] = score(question_vec, fact_vec, embedding_fact)

    # get the line number of the candidate fact with the highest score, and return it
    best_fact = max(fact_scores.iteritems(), key=operator.itemgetter(1))[0]
    return best_fact


def predict(stories, embedding_fact, embedding_answer, num_words):
    """ Predicts the answer and supporting fact for every question in a list of stories.  

    arguments:
        stories -- a list of stories
        embedding_fact -- the trained fact embedding matrix; a D X 2|W| matrix 
        embedding_answer -- the trained answer embedding matrix; a D X 3|W| matrix
        num_words -- the number of words in the vocabulary


    returns: a list of python dictionaries, one for each story.
             each dictionary holds entries of the form:
                 key:  question line number
                 value: a prediction object (see above)

    """

    predictions = []

    for story in stories:

        # the predictions for this story
        story_predictions = {}

        for (question_linenum, question) in story.questions.iteritems():

            # the "memory bank" for a question is a python dictionary containing the statements which have already appeared   
            # in the story before this question (that is, the statements which are candidates to be chosen as the supporting fact).
            # statements in the memory bank are indexed by line number in their story (1-15)
            memory_bank = {statement.linenum : statement.statement for statement in story.statements.itervalues() if statement.linenum < question_linenum}

            # predict the supporting fact, given the question, the memory bank, and the trained fact embedding matrix 
            predicted_fact = predict_fact(question.question, memory_bank, embedding_fact)

            # Fact (the correct supporting fact)
            #actual_fact = story.statements[question.fact_linenum]

            # predict the answer, given the question, supporting fact, and the trained answer embedding matrix
            predicted_answer = predict_answer(question.question, story.statements[predicted_fact].statement, num_words, embedding_answer)
            
            # save prediction for question in dictionary for this story, indexed by question's line number in story
            story_predictions[question_linenum] = Prediction(predicted_fact, predicted_answer)

        predictions.append(story_predictions) # add all predictions for this story to list of story predictions

    return predictions


def evaluate(stories, predictions, num_words):
    """ Evaluate the accuracy of a memory network's predictions.

    arguments:
        stories -- a list of stories
        predictions -- a list of corresponding predictions
        num_words (int) -- the number of words in the vocabulary 

    returns: 
        the accuracy of the supporting fact predictions
        the accuracy of the answer predictions
        the baseline accuracy of the supporting fact predictions (under a random guessing strategy)
        the baseline accuracy of the answer predictions (under a random guessing strategy)

    """


    num_fact_correct = 0.0  # number of fact predictions that were correct
    num_answer_correct = 0.0 # number of answer predictions that were correct
    baseline_num_fact_correct = 0.0 # baseline number of fact predictions that would be correct by chance

    num_total = 0.0 # number of total questions / predictions

    for (story, story_predictions) in zip(stories, predictions):
        for (question_linenum, prediction) in story_predictions.iteritems():
            actual_fact_linenum = story.questions[question_linenum].fact_linenum # index of correct fact
            actual_answer_index = story.questions[question_linenum].answer_index # index of correct answer

            if actual_fact_linenum == prediction.fact_linenum: # check if fact was correct
                num_fact_correct += 1

            if actual_answer_index == prediction.answer_index: # check if answer was correct
                num_answer_correct += 1

            # the "memory bank" size is the number of statements that have been given thus far in the story
            memory_bank_size = len([1 for statement in story.statements.itervalues() if statement.linenum < question_linenum])
            
            # expected value if we chose a fact by randomly choosing one from the memory bank
            baseline_num_fact_correct += 1.0 / memory_bank_size

            num_total += 1

    # compute actual and baseline accuracies from correct counts
    fact_accuracy = num_fact_correct / num_total
    answer_accuracy = num_answer_correct / num_total
    baseline_fact_accuracy = baseline_num_fact_correct / num_total
    baseline_answer_accuracy = 1.0 / num_words

    return (fact_accuracy, answer_accuracy, baseline_fact_accuracy, baseline_answer_accuracy)

def compute_accuracy(examples, embedding):
    """ Computes accuracy on an embedding model (either fact or answer prediction),
    used for tracking training accuracy on each model.
    
    NOTE: this computes only the accuracy on half of the overall model (i.e.
    computes fact accuracy given question, or answer accuracy given correct fact),
    and does not chain together fact and answer prediction.
    
    arguments: 
        examples (list) -- set of examples on which to test the embedding
        embedding -- the trained fact or answer embedding matrix, a D X 3|W| matrix 
        
    returns:
        accuracy of the embedding
    """
    
    num_correct = 0.0
    num_total = 0.0

    for (query_vec, correct_vec, wrong_vecs) in examples:
        # list of scores for all wrong answers
        wrong_scores = [score(query_vec, wrong_vec, embedding) for wrong_vec in wrong_vecs]
        # correct score
        correct_score = score(query_vec, correct_vec, embedding)
        if correct_score > max(wrong_scores): # correct if correct answer scored higher than all others
            num_correct += 1
        num_total += 1.0

    accuracy = num_correct / num_total

    return accuracy    

def train(train_examples, test_examples, num_words, embed_dimension, num_bags, margin, init_learning_rate, 
    learning_rate_decay, weight_decay, momentum, initial_weights_scale, num_epochs):
    """Trains the embedding matrix for either the fact or answer embedding using batch gradient descent.
    
    arguments:
        train_examples (list) -- examples to train on
        test_examples (list) -- examples to test on
        num_words (int) -- number words in dictionary, |W|
        embed_dimension (int) -- dimension in which to embed
        num_bag_words (int) -- the number of bag of words representations in the embedding (2 for fact, and 3 for answer)
        margin (float) -- margin for the margin ranking loss function
        init_learning_rate (float) -- initial learning rate for updates
        learning_rate_decay (float) -- learning rate decays each epoch, with value 1/(1+learning_rate_decay*epoch_num) of intial
        weight_decay (float) -- amount of weight decay to use on entries in the embedding matrix
        momentum (float) -- amount of momentum to use on embedding matrix
        initial_weights_scale (float) -- mean of normal distribution that is used to initialize embedding matrix
        num_epochs (int) --  number of epochs for which to train embedding
        
    returns:
        trained embedding matrix, matrix of D x num_bags*|W| size 
    
    """ 

    embedding = np.random.normal(scale=initial_weights_scale, size=[embed_dimension, num_words * num_bags]) 

    train_accuracy = compute_accuracy(train_examples, embedding) # initial embedding accuracy on training examples
    test_accuracy = compute_accuracy(test_examples, embedding) # initial embedding accuracy on testing examples

    print "initial epoch"
    print "\ttrain accuracy: %f\n\ttest accuracy: %f" % (train_accuracy, test_accuracy)
    
    for epoch in range(num_epochs):
        loss = 0
        gradient = np.zeros([embed_dimension, num_words * num_bags]) # initializes gradient of error
        delta = np.zeros([embed_dimension, num_words * num_bags]) # initializes weight update

        # computes summmed loss and gradient for all training examples        
        for (query_vec, correct_vec, wrong_vecs) in train_examples:
            correct_score = score(query_vec, correct_vec, embedding)
            correct_gradient = score_deriv(query_vec, correct_vec, embedding)

            for wrong_vec in wrong_vecs:
                loss_term = margin - correct_score + score(query_vec, wrong_vec, embedding)

                if loss_term > 0: # if difference between wrong and correct score less than margin
                    loss += loss_term
                    gradient += -1 * correct_gradient + score_deriv(query_vec, wrong_vec, embedding)

        # decreases learning_rate each epoch by learnin_rate_decay
        step_size = init_learning_rate / (1 + epoch * learning_rate_decay)    
        # amount to update embedding. is step_size*gradient, with momentum and weight decay terms
        delta = (-1 * step_size * gradient) + (momentum * delta) - (step_size * weight_decay * embedding)

        embedding += delta # updates embedding

        train_accuracy = compute_accuracy(train_examples, embedding) # computes embedding accuracy each epoch on train set
        test_accuracy = compute_accuracy(test_examples, embedding) # computes embedding accuracy each epoch on test set
        print "epoch %d" % epoch
        print "\tloss: %f\n\ttrain accuracy: %f\n\ttest accuracy: %f" % (loss, train_accuracy, test_accuracy)

    return embedding # return trained embedding matrix

def train_fact(train_stories, test_stories, num_words, embed_dimension=10, num_bags=2, margin=0.1, init_learning_rate=1e-3, 
    learning_rate_decay=0.1, weight_decay=0.05, momentum=0.1, initial_weights_scale=0.001, num_epochs=30):  
    """Calls train function to train the fact embedding matrix. 
    
    arguments: values of the train function parameters (see above), with set defaults.
    returns: trained fact embedding matrix
    """
    def make_examples(stories):
        """Creates list of examples from list of stories. Each element of example 
        list is a tuple of form (question vector, correct fact vector, list of wrong fact vectors).
        
        argument: list of Story objects
        returns: list of examples    
        """
        examples = []

        for story in stories:
            for (question_linenum, question) in story.questions.iteritems():
                question_vec = form_vector(num_words, question=question.question)
                correct_fact_vec = form_vector(num_words, fact=story.statements[question.fact_linenum].statement)

                wrong_fact_vecs = []
                for (statement_linenum, statement) in story.statements.iteritems():
                    # if statement appears before question, and isn't the correct fact, append to wrong facts list
                    if statement_linenum < question_linenum and statement_linenum != question.fact_linenum:
                        wrong_fact_vec = form_vector(num_words, fact=statement.statement)
                        wrong_fact_vecs.append(wrong_fact_vec)
                        
                # add example to list of examples
                examples.append((question_vec, correct_fact_vec, wrong_fact_vecs))

        return examples

    train_examples = make_examples(train_stories) # create list of training examples
    test_examples = make_examples(test_stories) # create list of testing examples

    # get trained fact embedding matrix
    fact_embedding = train(train_examples, test_examples, num_words, embed_dimension, num_bags, margin, 
                            init_learning_rate, learning_rate_decay, weight_decay, momentum, 
                            initial_weights_scale, num_epochs)

    return fact_embedding

def train_answer(train_stories, test_stories, num_words, embed_dimension=10, num_bags=3, margin=0.1, init_learning_rate=1e-4, 
    learning_rate_decay=0, weight_decay=0.05, momentum=0.9, initial_weights_scale=0.001, num_epochs=100):
    """Calls train function to train the answer embedding matrix. 
    
    arguments: values of the train function parameters (see above), with set defaults.
    returns: trained answer embedding matrix
    """    
    
    def make_examples(stories):
        """Creates list of examples from list of stories. Each element of example 
        list is a tuple of form (question and fact vector, correct answer vector, list of wrong answer vectors).
        
        argument: list of Story objects
        returns: list of examples    
        """
        examples = []

        for story in stories:
            for (question_linenum, question) in story.questions.iteritems():
                question_vec = form_vector(num_words, question=question.question, fact=story.statements[question.fact_linenum].statement)
                correct_answer_vec = form_vector(num_words, answer_index=question.answer_index)
                # vector of all wrong answer vectors formed from all indices not equal to correct answer index                
                wrong_answer_vecs = [form_vector(num_words, answer_index=word_index) for word_index in range(num_words) if word_index != question.answer_index]

                # add example to list of examples                
                examples.append((question_vec, correct_answer_vec, wrong_answer_vecs))

        return examples

    train_examples = make_examples(train_stories) # create list of train examples
    test_examples = make_examples(test_stories) # create list of test examples

    # get trained answer embedding matrix
    answer_embedding = train(train_examples, test_examples, num_words, embed_dimension, num_bags, margin, 
                                init_learning_rate, learning_rate_decay, weight_decay, momentum, 
                                initial_weights_scale, num_epochs)

    return answer_embedding


train_filename = 'data/tasksv11/en/qa1_single-supporting-fact_train.txt'
test_filename = 'data/tasksv11/en/qa1_single-supporting-fact_test.txt'

if __name__ == '__main__':

    # read in the stories as raw text
    train_stories = load_stories(train_filename)
    test_stories = load_stories(test_filename)

    # create a dictionary, based on training AND testing data, that maps from words to indices
    [word_to_index, index_to_word] = make_dictionary(train_stories + test_stories)
    num_words = len(index_to_word)

    # transform the stories into word count vector representations
    for story in train_stories:
        story.vectorize(word_to_index)
    for story in test_stories:
        story.vectorize(word_to_index)

    # train the fact embedding matrix
    fact_embedding = train_fact(train_stories, test_stories, num_words)

    # train the answer embedding matris
    answer_embedding = train_answer(train_stories, test_stories, num_words)
    
    # computes full predictions and accuracy (i.e. chains together fact and answer prediction) on the training set
    train_predictions = predict(train_stories, fact_embedding, answer_embedding, num_words)
    [train_fact_accuracy, train_answer_accuracy, train_chance_fact, train_chance_answer] = evaluate(train_stories, train_predictions, num_words)
    
    # computes full predictions and accuracy (i.e. chains together fact and answer prediction) on the testing set    
    test_predictions = predict(test_stories, fact_embedding, answer_embedding, num_words)
    [test_fact_accuracy, test_answer_accuracy, test_chance_fact, test_chance_answer] = evaluate(test_stories, test_predictions, num_words)

    print "\n\t\taccuracy\n"
    print "\t\tfact task\tanswer task"
    print "training:\t%f\t%f" % (train_fact_accuracy, train_answer_accuracy)
    print "testing:\t%f\t%f" % (test_fact_accuracy, test_answer_accuracy)
