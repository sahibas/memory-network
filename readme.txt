Memory Networks for Question Answering
Sahiba Singh and Jeremy Cohen
May 17, 2015

In this assignment, we implemented a “memory network”[1] that performs a simple question answering task.

The sentences and questions are in the data folder. The file "full_memory_network.py" reads in the data, and creates this memory network. Further implementation details are given in "Memory_Network_readme.pdf".


[1] Weston J, Chopra S, Bordes A (2014) Memory networks. arXiv preprint
arXiv:14103916 .
